/**
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { Template } from 'meteor/templating'
import { Blaze } from 'meteor/blaze'
import '/imports/ui/components/overlay/overlay.js'

/**
 * Overlays holder
 */
Overlays = {
    /**
     * Closing opened overlay by key
     *
     * @param {string} key
     */
    close: function (key) {
        if (this[key] && typeof this[key] === 'object') {
            this[key].close();
        }
    },

    /**
     * Close all opened overlays.
     */
    closeAll: function () {
        $.each(this, this.close.bind(this));
    }
};

export class Overlay {

    /**
     * Provide: template, page_title.
     *
     * @param data
     */
    constructor (data) {
        this.data = data;
        this.open();
    }

    open () {
        this.data.overlay = this;
        this.template = Blaze.renderWithData(Template['components_overlay'], this.data, document.body);

        /* Add overlay to overlays registry */
        Overlays[this.data.template] = this;
    }

    /**
     * Overlay close action.
     */
    close () {
        Blaze.remove(this.template);

        /* Remove overlay from registry */
        if (Overlays[this.data.template]) {
            delete(Overlays[this.data.template]);
        }
    }

}