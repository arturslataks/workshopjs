/**
 * Globally useful functions
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { ReactiveVar } from 'meteor/reactive-var'

/**
 * Takes field values and makes key: value object.
 *
 * @param form
 * @param template
 */
export const formToObject = function (form, template) {
    form = template ? template.$(form) : $(form);

    return _.object(_.map(form.serializeArray(), (field) => {
        return [field.name, field.value]
    }));
};

/**
 * If object contains ReactiveVars in it - before sending to server get value.
 */
export const flatten = function (object) {
    _.each(object, (item, key) => {
        if (item instanceof ReactiveVar) {
            object[key] = item.get();
        }
    });

    return object;
};
