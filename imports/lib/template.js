/**
 * Template helpers file.
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'

/**
 * Returns url for specified path. See example below for
 * reference: Calling from template {{ urlFor 'login' param = 'value' }}
 *
 * @param {string} path
 * @param {{}} params
 * @returns {string} Url
 */
Template.registerHelper('urlFor', function (path, params) {
    return FlowRouter.path(path, params.hash);
});

/**
 * Format date to global format
 */
Template.registerHelper('date', function (date) {
    return moment(date).format('MMMM Do YYYY')
});

/**
 * Format currency to global format
 */
Template.registerHelper('currency', function (amount) {
    return 'EUR ' + parseFloat(amount || 0).toFixed(2);
});
