/**
 * Routes definition
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/* Import components required for application startup */
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { BlazeLayout } from 'meteor/kadira:blaze-layout'

/* Import templates for routes definition  */
import '/imports/ui/layouts/base.js'
import '/imports/ui/pages/main_screen.js'
import '/imports/ui/pages/auth/login.js'
import '/imports/ui/pages/auth/register.js'

/* Application base layout */
const baseLayout = 'layouts_base';

/* Instead of pasting page content into __blaze-root div we will put into body */
BlazeLayout.setRoot('body');

const protectedRoutesGroup = FlowRouter.group({
    triggersEnter: [function () {
        if (!Meteor.userId()) {
            FlowRouter.go('login')
        }
    }]
});

const guestOnlyRoutesGroup = FlowRouter.group({
    triggersEnter: [function () {
        if (Meteor.userId()) {
            FlowRouter.go('home')
        }
    }]
});

/* Routes definition goes here then */
protectedRoutesGroup.route('/', {
    name: 'main',
    action () {
        BlazeLayout.render(baseLayout, { main: 'pages_main_screen', page_title: 'Welcome' });
    }
});

protectedRoutesGroup.route('/logout', {
    name: 'logout',
    action () {
        Meteor.logout(() => {
            FlowRouter.go('login');
        })
    }
});

guestOnlyRoutesGroup.route('/login', {
    name: 'login',
    action () {
        BlazeLayout.render(baseLayout, { main: 'pages_auth_login', page_title: 'Login' });
    }
});

guestOnlyRoutesGroup.route('/register', {
    name: 'register',
    action () {
        BlazeLayout.render(baseLayout, { main: 'pages_auth_register', page_title: 'Register' });
    }
});
