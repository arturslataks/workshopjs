/**
 * Import startup client code in one file.
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

// Register your APIs here

import '/imports/api/grocery_lists/server'
