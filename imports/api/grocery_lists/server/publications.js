/**
 * Publications for Grocery lists collection
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { GroceryLists } from '../lists.js'
import { Meteor } from 'meteor/meteor'

Meteor.publish('grocery_lists.all', function () {
    return GroceryLists.find({ user: this.userId }, { limit: Meteor.settings.listsLimit, sort: { createdAt: -1 } });
});