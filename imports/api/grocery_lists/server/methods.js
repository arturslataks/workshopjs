/**
 * Server side methods for Grocery lists collection
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { Meteor } from 'meteor/meteor'
import { GroceryLists } from '../lists.js'
import { check } from 'meteor/check'

Meteor.methods({

    'grocery_lists.new' (data) {
        if (!this.userId) return;

        check(data, { items: [String] });

        if (!data.items.length) {
            throw new Meteor.Error('Error reason', 'Error description');
        }

        GroceryLists.insert(_.extend(data, {
            createdAt: new Date(),
            user: this.userId,
            isCurrent: true
        }));
    },

    'grocery_lists.remove' (id) {
        if (!this.userId) return;

        GroceryLists.remove({ user: this.userId, _id: id });
    },

    'grocery_lists.complete' (id) {
        if (!this.userId) return;

        GroceryLists.update({ user: this.userId, _id: id }, { $set: { isCurrent: false } });
    }

});
