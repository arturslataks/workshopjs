/**
 * Grocery lists collection definition
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { Mongo } from 'meteor/mongo';

export const GroceryLists = new Mongo.Collection('grocery_lists');