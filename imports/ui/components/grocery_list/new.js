/**
 * Grocery list new entry JS file
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import { flatten } from '/imports/lib/functions.js'
import './new.html'

Template.components_grocery_list_new.events({

    'click .add-item' (e, template) {
        e.preventDefault();

        var items = template.list.items.get();
        const input = template.$('#grocery-list-item');

        if (input.val()) {
            items.push(input.val());
            template.list.items.set(items);
            input.val('');
        }
    },

    'submit #grocery-list-form' (e, template) {
        e.preventDefault();

        Meteor.call('grocery_lists.new', flatten(template.list), (error, result) => {
            if (error) {
                console.log(error);
            } else {
                template.data.overlay.close();
            }
        })
    }

});

Template.components_grocery_list_new.helpers({

    groceryList () {
        const list = Template.instance().list;

        if (list.items.get().length) {
            return list;
        }
    }

});

Template.components_grocery_list_new.onCreated(function () {

    this.list = {
        items: new ReactiveVar([])
    };

});