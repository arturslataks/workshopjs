/**
 * Grocery list preview template
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { Meteor } from 'meteor/meteor'
import { ReactiveVar } from 'meteor/reactive-var'
import { Template } from 'meteor/templating'
import './preview.html'

Template.components_grocery_list_preview.events({

    'click .remove-button' (e, template) {
        e.preventDefault();

        if (confirm('You sure?')) {
            if (template.data._id) {
                Meteor.call('grocery_lists.remove', template.data._id);
            } else {
                template.data.items.set([]);
            }
        }
    },

    'click .mark-button' (e, template) {
        e.preventDefault();

        Meteor.call('grocery_lists.complete', template.data._id);
    }

});

Template.components_grocery_list_preview.helpers({

    items () {
        if (this.items instanceof ReactiveVar) {
            return this.items.get();
        } else {
            return this.items;
        }
    }

});
