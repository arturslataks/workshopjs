/**
 * Grocery list new entry JS file
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import './overlay.html'

Template.components_overlay.events({

    /**
     * Close add catch overlay
     */
    'click .back-button' (e, template) {
        e.preventDefault();

        template.data.overlay.close();
    }

});