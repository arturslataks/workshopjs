/**
 * Application main screen JS file
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { Template } from 'meteor/templating'
import { GroceryLists } from '/imports/api/grocery_lists/lists.js'
import { Overlay } from '/imports/lib/overlay.js'
import { Meteor } from 'meteor/meteor'

import './main_screen.html'
import '/imports/ui/components/grocery_list/preview.js'
import '/imports/ui/components/grocery_list/new.js'
import '/imports/ui/components/nothing_found.html'

Template.pages_main_screen.events({

    'click .add-list' (e, template) {
        e.preventDefault();

        new Overlay({ template: 'components_grocery_list_new', page_title: 'Add New Grocery List'})
    }

});

Template.pages_main_screen.helpers({

    welcomeMessage () {
        const user = Meteor.user();

        return user ? 'Welcome, ' + Meteor.user().profile.name : 'Welcome';
    },

    helperMessage () {
        return Meteor.settings.public.mainPageHelperText;
    },

    currentGroceryList () {
        return GroceryLists.findOne({ isCurrent: true });
    },

    lists () {
        return GroceryLists.find({ isCurrent: { $ne: true } }, { sort: { createdAt: -1 }});
    }

});

Template.pages_main_screen.onCreated(function () {

    this.subscribe('grocery_lists.all');

});
