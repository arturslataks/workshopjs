/**
 * Application login page JS file.
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { Meteor } from 'meteor/meteor'
import { Template } from 'meteor/templating'
import { formToObject } from '/imports/lib/functions.js'

import './login.html'

Template.pages_auth_login.events({

    'submit #login-form' (e, template) {
        e.preventDefault();

        const data = formToObject(e.currentTarget);

        Meteor.loginWithPassword(data.username, data.password, (error) => {
            if (error) console.log(error); else FlowRouter.go('main');
        })
    }

});
