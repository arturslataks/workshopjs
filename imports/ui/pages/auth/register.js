/**
 * Application register page JS file.
 *
 * @author Arturs Lataks <info@scandiweb.com>
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

import { Template } from 'meteor/templating'
import { formToObject } from '/imports/lib/functions.js'
import { Accounts } from 'meteor/accounts-base'

import './register.html'

Template.pages_auth_register.events({

    'submit #register-form' (e, template) {
        e.preventDefault();

        const data = formToObject(e.currentTarget);
        data.profile = { name: data.name, dob: new Date(data.dob) };

        Accounts.createUser(data, (error) => {
            if (error) console.log(error); else FlowRouter.go('main');
        });
    },

    'focus #dob' (e, template) {
        if (Meteor.isCordova) {
            const input = $(e.currentTarget);

            // Remove focus from input so ios does not show additional "Done" which might be confusing.
            input.blur();

            datePicker.show({ date: new Date(), mode: 'date' }, (result) => {
                input.val(moment(result).format('DD-MM-YYYY'))
            }, () => {});
        }
    }

});
